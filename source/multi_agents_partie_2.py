import random

class Puzzle():
    def __init__(self, taille : int) -> None:
        self.taille = taille
        self.grille = [[0] * self.taille for _ in range(self.taille)]

    def update_grille(self, old_position, new_position): 
        old_x, old_y = old_position
        self.grille[old_x][old_y] = 0

        new_x, new_y = new_position
        self.grille[new_x][new_y] = 1

    def affichage_grille(self):
        print("Affichage grille :")
        for ligne in self.grille:
            print(ligne)


class Agent():
    def __init__(self, name : str,position : tuple, case_cible : tuple, puzzle : Puzzle ) -> None:
        self.name = name
        self.succes = False
        self.pos_x , self.pos_y = position
        self.cible_x, self.cible_y = case_cible

        if (0<= self.pos_x < puzzle.taille) and (0<= self.pos_y < puzzle.taille) and (puzzle.grille[self.pos_x][self.pos_y] == 0):
            puzzle.grille[self.pos_x][self.pos_y] = 1
            
        if not ((0<= self.cible_x < puzzle.taille) and (0<= self.cible_y < puzzle.taille)):
            raise Exception(f'position invalide pour la case cible : {case_cible}')
        
    def get_cible(self):
        return(self.cible_x, self.cible_y)

    def get_position(self):
        return(self.pos_x, self.pos_y)
    
    def update_position(self, position : tuple, puzzle : Puzzle):
        puzzle.update_grille(old_position = self.get_position(), new_position = position)
        self.pos_x , self.pos_y = position      


# Fonction pour parcourir les cases voisines et trouver une case libre
def trouver_voisins_libres(puzzle : Puzzle, agent : Agent):
    pos_x, pos_y = agent.get_position()
    voisins = [(pos_x-1, pos_y), (pos_x+1, pos_y), (pos_x, pos_y-1), (pos_x, pos_y+1)]  # Coordonnées des cases voisines
    
    voisins_libres = []
    for voisin_x, voisin_y in voisins:
        if (0 <= voisin_x < puzzle.taille) and (0 <= voisin_y < puzzle.taille):
            if puzzle.grille[voisin_x][voisin_y] == 0:  # Case libre
                voisins_libres.append((voisin_x, voisin_y))

    return voisins_libres

# Fonction pour parcourir les cases voisines et trouver le meilleur chemin
def trouver_meilleur_case(puzzle: Puzzle, agent: Agent):
    if agent.get_position() == agent.get_cible():
        return agent.get_cible()
    else:
        pos_x, pos_y = agent.get_position()
        cible_x, cible_y = agent.get_cible()
        voisins = [(pos_x-1, pos_y), (pos_x+1, pos_y), (pos_x, pos_y-1), (pos_x, pos_y+1)]
        
        meilleur_voisins = []
        for voisin_x, voisin_y in voisins:
            if (0 <= voisin_x < puzzle.taille) and (0 <= voisin_y < puzzle.taille):
                meilleur_voisins.append((voisin_x, voisin_y))
            
        # Tri des voisins_libres en fonction de leur distance à la case cible
        meilleur_voisins = sorted(meilleur_voisins, key=lambda meilleur_voisin: abs(meilleur_voisin[0] - cible_x) + abs(meilleur_voisin[1] - cible_y))
    
        return meilleur_voisins[0]  # Retourne le meilleur voisin 

class Message():
    def __init__(self) -> None:
        self.position = (-1,-1)

    def get_request_move(self):
        return self.position
    
    def request_move(self, position):
        self.position = position

    
def deplacement_agent(puzzle : Puzzle, agent : Agent, message : Message):

    position = agent.get_position()
    case_cible = agent.get_cible()
    meilleur_case = trouver_meilleur_case(puzzle, agent)
    voisins_libres = trouver_voisins_libres(puzzle, agent)
    request_move = message.get_request_move()

    if position == request_move:
        agent.succes = False
        if meilleur_case in voisins_libres:
            agent.update_position(meilleur_case, puzzle)
            print(f"L'agent {agent.name} à la position {position} a choisi la case {meilleur_case}.")
            message.__init__()
        else:          
            case = random.choice(voisins_libres)
            agent.update_position(case, puzzle)
            print(f"L'agent {agent.name} à la position {position} a choisi la case {case}.")
            message.__init__()
    else:

        if meilleur_case in voisins_libres:
            agent.update_position(meilleur_case, puzzle)
            print(f"L'agent {agent.name} à la position {position} a choisi la case {meilleur_case}.")
        else:
            if meilleur_case == position == case_cible:
                agent.succes = True
                print(f"L'agent {agent.name} à atteint se cible")
            else:
                message.request_move(meilleur_case)
                print(f"L'agent {agent.name} à demander un changement la position{meilleur_case}")
         
if __name__ == "__main__":
    puzzle = Puzzle(5)
    
    etoile = Agent("etoile", (2,2), (2,2), puzzle)
    carre = Agent("carré", (2,4), (2,3), puzzle)
    triangle = Agent("triangle", (4,2), (0,2), puzzle)

    agents  = [etoile, carre, triangle]

    message = Message()
    
    puzzle.affichage_grille()

while True:
    for agent in agents:
        deplacement_agent(puzzle, agent, message)
    if (etoile.succes == True) and (carre.succes == True) and (triangle.succes == True):
        print("Puzzle réaliser")
        break 
    puzzle.affichage_grille()